import { Component, ElementRef, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import * as THREE from "three";
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { OBJLoader  } from 'three/examples/jsm/loaders/OBJLoader.js';
import { TransformControls } from 'three/examples/jsm/controls/TransformControls';

@Component({
  selector: 'app-model2',
  templateUrl: './model2.component.html',
  styleUrls: ['./model2.component.scss']
})
export class Model2Component implements OnInit {

  // https://github.com/mrdoob/three.js/blob/master/examples/misc_controls_transform.html

  renderer!: THREE.WebGLRenderer;
  cameraPersp!: THREE.PerspectiveCamera;
  cameraOrtho!: THREE.OrthographicCamera;
  currentCamera!: any;
  scene!: THREE.Scene;
  loaderGLTF =  new GLTFLoader();
  orbit!: OrbitControls;
  control!: TransformControls;
  loaderOBJ = new OBJLoader();

  constructor() { }

  async ngOnInit() {

    
  }

  ngAfterViewInit() {
    this.init();
  }

  async init() {
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild( this.renderer.domElement );    

    const aspect = window.innerWidth / window.innerHeight;
    this.cameraPersp = new THREE.PerspectiveCamera(50, aspect, 0.01, 30000);
    this.cameraOrtho = new THREE.OrthographicCamera( - 600 * aspect, 600 * aspect, 600, - 600, 0.01, 30000 );
    this.currentCamera = this.cameraPersp;

    this.currentCamera.position.set( 1000, 500, 1000 );
    this.currentCamera.lookAt( 0, 200, 0 );
    
    this.scene = new THREE.Scene();
    this.scene.add( new THREE.GridHelper( 1000, 10, 0x888888, 0x444444 ) );

    const light = new THREE.DirectionalLight( 0xffffff, 2 );
    light.position.set( 1, 1, 1 );
    this.scene.add( light );

    const glb = await this.loaderGLTF.loadAsync('../../assets/model.glb');
    let model = glb.scene.children[0];

    const obj = await this.loaderOBJ.loadAsync('../../assets/Dish/Dish 1.obj');

    let box = new THREE.Box3().setFromObject(model);
    box.getCenter(model.position); // this re-sets the mesh position
    model.position.multiplyScalar(-1);

    this.orbit = new OrbitControls( this.currentCamera, this.renderer.domElement );
    this.orbit.update();
    this.orbit.addEventListener( 'change', this.render );

    this.control = new TransformControls( this.currentCamera, this.renderer.domElement );
    this.control.addEventListener( 'change', this.render );

    this.control.addEventListener( 'dragging-changed', ( event ) => {

      this.orbit.enabled = ! event['value'];

    } );

    this.scene.add(model);
    this.scene.add(obj)
    this.control.attach(obj);
    this.scene.add(this.control);

    this.render();

    window.addEventListener( 'resize', this.onWindowResize );

    window.addEventListener( 'keydown',  ( event ) => {

      switch ( event.keyCode ) {

        case 81: // Q
          this.control.setSpace( this.control.space === 'local' ? 'world' : 'local' );
          break;

        case 16: // Shift
          this.control.setTranslationSnap( 100 );
          this.control.setRotationSnap( THREE.MathUtils.degToRad( 15 ) );
          this.control.setScaleSnap( 0.25 );
          break;

        case 87: // W
          this.control.setMode( 'translate' );
          break;

        case 69: // E
          this.control.setMode( 'rotate' );
          break;

        case 82: // R
          this.control.setMode( 'scale' );
          break;

        case 67: // C
          const position = this.currentCamera.position.clone();

          this.currentCamera = this.currentCamera.isPerspectiveCamera ? this.cameraOrtho : this.cameraPersp;
          this.currentCamera.position.copy( position );

          this.orbit.object = this.currentCamera;
          this.control.camera = this.currentCamera;

          this.currentCamera.lookAt( this.orbit.target.x, this.orbit.target.y, this.orbit.target.z );
          this.onWindowResize();
          break;

        case 86: // V
          const randomFoV = Math.random() + 0.1;
          const randomZoom = Math.random() + 0.1;

          this.cameraPersp.fov = randomFoV * 160;
          this.cameraOrtho.bottom = - randomFoV * 500;
          this.cameraOrtho.top = randomFoV * 500;

          this.cameraPersp.zoom = randomZoom * 5;
          this.cameraOrtho.zoom = randomZoom * 5;
          this.onWindowResize();
          break;

        case 187:
        case 107: // +, =, num+
          this.control.setSize( this.control.size + 0.1 );
          break;

        case 189:
        case 109: // -, _, num-
          this.control.setSize( Math.max( this.control.size - 0.1, 0.1 ) );
          break;

        case 88: // X
          this.control.showX = ! this.control.showX;
          break;

        case 89: // Y
          this.control.showY = ! this.control.showY;
          break;

        case 90: // Z
          this.control.showZ = ! this.control.showZ;
          break;

        case 32: // Spacebar
          this.control.enabled = ! this.control.enabled;
          break;

        case 27: // Esc
          this.control.reset();
          break;

      }

    } );

    window.addEventListener( 'keyup', ( event ) => {

      switch ( event.keyCode ) {

        case 16: // Shift
          this.control.setTranslationSnap( null );
          this.control.setRotationSnap( null );
          this.control.setScaleSnap( null );
          break;

      }

    } );
  }
  onWindowResize() {

    const aspect = window.innerWidth / window.innerHeight;

    this.cameraPersp.aspect = aspect;
    this.cameraPersp.updateProjectionMatrix();

    this.cameraOrtho.left = this.cameraOrtho.bottom * aspect;
    this.cameraOrtho.right = this.cameraOrtho.top * aspect;
    this.cameraOrtho.updateProjectionMatrix();

    this.renderer.setSize( window.innerWidth, window.innerHeight );

    this.render();

  }

  render() {
    if (this.scene && this.currentCamera) {
      console.log(this.scene, this.currentCamera)

      this.renderer.render( this.scene, this.currentCamera );

    }

  }
}
