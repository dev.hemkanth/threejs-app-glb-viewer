import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModelComponent } from './model/model.component';
import { Model2Component } from './model2/model2.component';
import { Model3Component } from './model3/model3.component';

const routes: Routes = [
  {
    path: '',
    component: ModelComponent
  },
  {
    path: 'model-2',
    component: Model2Component
  },
  {
    path: 'model-3',
    component: Model3Component
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
