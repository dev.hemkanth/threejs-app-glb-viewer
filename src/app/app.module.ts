import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModelComponent } from './model/model.component';
import { NgxJoystickModule } from 'ngx-joystick';
import { Model2Component } from './model2/model2.component';
import { Model3Component } from './model3/model3.component'; 
@NgModule({
  declarations: [
    AppComponent,
    ModelComponent,
    Model2Component,
    Model3Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgxJoystickModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
