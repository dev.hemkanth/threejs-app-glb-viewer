import { Component, ElementRef, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import * as THREE from "three";
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { CSS2DRenderer } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import { HemisphereLight, sRGBEncoding } from 'three';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';
import { OBJLoader  } from 'three/examples/jsm/loaders/OBJLoader.js';
import { DragControls } from 'three/examples/jsm/controls/DragControls';


import { JoystickEvent, NgxJoystickComponent } from 'ngx-joystick';
import { JoystickManagerOptions, JoystickOutputData } from 'nipplejs';


@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.scss']
})
export class ModelComponent implements OnInit, AfterViewInit {
  

  @ViewChild('staticJoystic') staticJoystick!: NgxJoystickComponent;

  @ViewChild('canvas') private canvasRef!: ElementRef;
  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  private camera!: THREE.PerspectiveCamera;

  private controls!: OrbitControls;

  private ambientLight!: THREE.AmbientLight;

  private light!: THREE.PointLight;

  private directionalLight!: THREE.DirectionalLight;

  private loaderGLTF = new GLTFLoader();

  private dracoLoader = new DRACOLoader();

  private loaderOBJ = new OBJLoader();


  private renderer!: THREE.WebGLRenderer;

  private scene!: THREE.Scene;

  private model: any;

  objects: any = [];

  enableRotate = false;


  staticOptions: JoystickManagerOptions = {
    mode: 'static',
    position: { left: '50%', top: '50%' },
    color: 'blue',
  };

  staticOutputData!: JoystickOutputData;
  directionStatic!: string;
  interactingStatic!: boolean;

  constructor() { 

  }

  ngOnInit(): void {
   
  }

  ngAfterViewInit() {

    // Create scene
    this.createScene();
    // Start rendering
    this.startRendering();
    // Add controls
    this.createControls();
  }
 

  // Create scene
  private createScene() {

    // Scene
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0xd4d4d8)

    // https://s3-us-west-2.amazonaws.com/s.cdpn.io/39255/ladybug.gltf
    // ../../assets/model.glb
    // this.dracoLoader.setDecoderPath( '/examples/js/libs/draco/' );
    this.loaderGLTF.setDRACOLoader( this.dracoLoader );

    this.loaderGLTF.load('../../assets/model.glb', (gltf: GLTF) => {
      this.model = gltf.scene.children[0];
      console.log(this.model);
      var box = new THREE.Box3().setFromObject(this.model);
      box.getCenter(this.model.position); // this re-sets the mesh position
      this.model.position.multiplyScalar(-1);
      this.scene.add(this.model);

      this.loaderOBJ.load('../../assets/Dish/Dish 1.obj', (obj) => {

      
        const group = new THREE.Group();
        group.add( obj );

        this.scene.add(group);

        this.objects.push(group);

        console.log(THREE, this.scene, this.objects)
      })


    });


    // Camera
    let aspectRatio = this.canvas.clientWidth / this.canvas.clientHeight;
    this.camera = new THREE.PerspectiveCamera(
     
    )
    this.camera.position.x = 100;
    this.camera.position.y = 100;
    this.camera.position.z = 100;

    const hemiLight = new HemisphereLight();
    hemiLight.name = 'hemi_light';
    this.scene.add(hemiLight);
    
    this.ambientLight = new THREE.AmbientLight(0xffffff, 0.3);
    this.scene.add(this.ambientLight);
    this.directionalLight = new THREE.DirectionalLight(0xffffff, 2.5);
    this.directionalLight.position.set(0.5, 0, 0.866);
    this.scene.add(this.directionalLight);
    // this.light = new THREE.PointLight(0x4b371c, 10);
    // this.light.position.set(0, 200, 400);
    // this.scene.add(this.light);

  }


  private startRendering() {
  
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias: true });
    this.renderer.physicallyCorrectLights = true;
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    let component: ModelComponent = this;
    (function render() {
      component.renderer.render(component.scene, component.camera);
      requestAnimationFrame(render);
    }());
  }

  private createControls = () => {
    const renderer = new CSS2DRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.domElement.style.position = 'absolute';
    renderer.domElement.style.top = '0px';
    document.body.appendChild(renderer.domElement);
    this.controls = new OrbitControls(this.camera, renderer.domElement);
   
    this.controls.autoRotate = false;
    this.controls.autoRotateSpeed = -10;
    this.controls.screenSpacePanning = true;
    this.controls.update();

    let dragControls = new DragControls( this.objects, this.camera, renderer.domElement );
    dragControls.addEventListener( 'dragstart',  ( event ) => { this.controls.enabled = false; } );
    dragControls.addEventListener( 'dragend',  ( event ) => { this.controls.enabled = true; } );

    
  };

  toggleRotate() {
    this.enableRotate = !this.enableRotate;

    if (this.enableRotate) {
      this.controls.enabled = false;
      console.log(this.renderer.domElement)
      this.renderer.domElement?.addEventListener('mousemove', event => {
        console.log(event)
        // let obj = this.scene.children.find(c => c.uuid == this.objects[0].uuid);
        // if (obj) obj.rotation.y += event.movementX * 0.005;
      })
    } else {
      this.controls.enabled = true;
    }
  }


  onStartStatic(event: JoystickEvent) {
    this.interactingStatic = true;
    console.log("listner")

    let obj:any = this.scene.children.find(c => c.uuid == this.objects[0].uuid);
    window.addEventListener('mousemove', event => {
      console.log("listner")
      obj.rotation.y += event.movementX * 0.005;
      obj.rotation.x += event.movementY * 0.005;

    })
  }

  onEndStatic(event: JoystickEvent) {
    this.interactingStatic = false;
    window.removeEventListener('mousemove', (e) => {"Removed listner"} )
  }

  onMoveStatic(event: JoystickEvent) {
    this.staticOutputData = event.data;

    // let obj:any = this.scene.children.find(c => c.uuid == this.objects[0].uuid);
    // if (obj) {
    //   if (this.staticOutputData.direction.angle == 'right' || this.staticOutputData.direction.angle == 'left' ) {
    //    if (this.staticOutputData.direction.angle == 'right') obj.rotation.x = (obj.rotation.x + 1) * 0.05;
    //    if (this.staticOutputData.direction.angle == 'left') obj.rotation.x = (obj.rotation.x - 1) * 0.05;

    //   } else if (this.staticOutputData.direction.angle == 'up' || this.staticOutputData.direction.angle == 'down' ) {
    //     if (this.staticOutputData.direction.angle == 'up') obj.rotation.y = (obj.rotation.y + 1) * 0.05;
    //     if (this.staticOutputData.direction.angle == 'down') obj.rotation.y = (obj.rotation.y - 1) * 0.05;
 
    //   }
    //   console.log(this.staticOutputData,obj)

    // }


  }

  onPlainUpStatic(event: JoystickEvent) {
    // let obj:any = this.scene.children.find(c => c.uuid == this.objects[0].uuid);
    // if (obj) {

    //   // while (this.interactingStatic) {
    //   //   // obj.rotation.y = (obj.rotation.y + 1) * 0.05;
    //   //   console.log("Yes")
    //   // };
    // }
  }

  onPlainDownStatic(event: JoystickEvent) {
    // let obj:any = this.scene.children.find(c => c.uuid == this.objects[0].uuid);
    // if (obj) {
    //   obj.rotation.y = (obj.rotation.y - 1) * 0.05;
    // }
  }

  onPlainLeftStatic(event: JoystickEvent) {
    // let obj:any = this.scene.children.find(c => c.uuid == this.objects[0].uuid);
    // if (obj) {
    //   obj.rotation.x = (obj.rotation.x - 1) * 0.05;
    // }
  }

  onPlainRightStatic(event: JoystickEvent) {
    // let obj:any = this.scene.children.find(c => c.uuid == this.objects[0].uuid);
    // if (obj) {
    //   obj.rotation.x = (obj.rotation.x + 1) * 0.05;
    // }
  }
  
// https://codepen.io/navin_moorthy/embed/mdbLEpd?default-tab=html%2Cresult&theme-id=dark
// https://stackoverflow.com/questions/26836065/rotating-object-around-object-axis-three-js
}
