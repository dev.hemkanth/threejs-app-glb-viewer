import { Component, ElementRef, AfterViewInit, OnInit, ViewChild } from '@angular/core';

import * as THREE from "three";
import {  sRGBEncoding } from 'three';
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { OBJLoader  } from 'three/examples/jsm/loaders/OBJLoader.js';
import { TransformControls } from 'three/examples/jsm/controls/TransformControls';

// import { InteractionManager } from "three.interactive";
import * as TWEEN from "@tweenjs/tween.js";
import gsap from "gsap";

@Component({
  selector: 'app-model3',
  templateUrl: './model3.component.html',
  styleUrls: ['./model3.component.scss']
})
export class Model3Component implements OnInit {

  @ViewChild('canvas') private canvasRef!: ElementRef;
  get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }
  

  cameraPersp!: THREE.PerspectiveCamera;
  cameraOrtho!: THREE.OrthographicCamera;
  currentCamera!: any;

  orbit!: OrbitControls;

  control!: TransformControls;

  ambientLight!: THREE.AmbientLight;

  directionalLight!: THREE.DirectionalLight;

  hemishpereLight!: THREE.HemisphereLight;

  loaderGLTF = new GLTFLoader();

  loaderOBJ = new OBJLoader();

  renderer!: THREE.WebGLRenderer;

  scene!: THREE.Scene;

  gridHelper!: THREE.GridHelper;
  isGridHelperReuired: boolean = false;

  model: any;

  objects: any = [];

  selectedModel!: any;
  enableControls:boolean = false;
  controllerType: string  = 'translate';

  componentTypes: Array<string> = ['Dish', 'Dish 1', 'Panel', 'Panel 2'];
  componentTypeToImport: string = 'Dish';
  @ViewChild('closeAddComponentModal') closeAddComponentModal!: any;

  interactionManager: any;

  glbRotateY = 0;


  // Extra
  showSidePanel: boolean = true;
  showTopRightPanel: boolean = true;
  showInfoModal: boolean = false;
  sidePanleData: 'SITE_DATA' | 'EQ_LIST' = 'EQ_LIST';

  constructor() { 

  }

  ngOnInit(): void {
   
  }

  ngAfterViewInit() {

    // Create scene
    this.createScene();
    // Start rendering
    this.startRendering();
    // Add controls
    this.createControls();
  }
 

  // Create scene
  createScene() {

    // Scene
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0xd4d4d8)

    // Grider Helper
    this.gridHelper = new THREE.GridHelper( 1000, 10, 0x888888, 0x444444 );
    this.gridHelper.name = 'GRID_HELPER';

    // GLB loader
    this.loaderGLTF.load('../../assets/model.glb', (gltf: GLTF) => {
      this.model = gltf.scene.children[0];
      let box = new THREE.Box3().setFromObject(this.model);
      box.getCenter(this.model.position);
      // this.model.position.multiplyScalar(-1);
      this.model.quaternion.x = 0;

      this.scene.add(this.model);
    });


    // Camera
    let aspectRatio = this.canvas.clientWidth / this.canvas.clientHeight;
    this.cameraPersp = new THREE.PerspectiveCamera(50, aspectRatio, 0.01, 30000);
   
    this.cameraOrtho = new THREE.OrthographicCamera( - 600 * aspectRatio, 600 * aspectRatio, 600, - 600, 0.01, 30000 );
    this.currentCamera = this.cameraPersp;
    this.currentCamera.position.set( 1000, 500, 1000 );
    this.currentCamera.lookAt( 0, 200, 0 );

    
    this.ambientLight = new THREE.AmbientLight(0xffffff);
    this.directionalLight = new THREE.DirectionalLight(0xffffff);
    this.hemishpereLight = new THREE.HemisphereLight(0xffffbb, 0x080820);
    // this.directionalLight.position.set(0.5, 0, 0.866);

 

    // this.directionalLight.position.copy(this.currentCamera.position);

    this.scene.add(this.ambientLight);
    this.scene.add(this.directionalLight);
    this.scene.add(this.hemishpereLight);


    
 
  }

  // Add component
  addComponent() {
    this.addOBJ();
    this.closeAddComponentModal.nativeElement.click();
  }

  

  // Add OBJ
  addOBJ() {

    const _assetPath = `../../assets/${this.componentTypeToImport}/${this.componentTypeToImport}.obj`
    this.loaderOBJ.load(_assetPath, (obj:any) => {

      obj.name = `${this.componentTypeToImport}_${this.objects.length + 1}`;
      obj.realName = this.componentTypeToImport;

      this.model.add(obj);
      this.objects.push(obj);
      // this.interactionManager.add(obj);
    })
  }

  removeOBJ(OBJ:any) {
    console.log(OBJ)
    const obj: any = this.scene.getObjectByName(OBJ.name);

    this.control.detach();
    this.scene.remove(this.control)


    this.model.remove(obj);

    this.objects = this.objects.filter((o:any) => o.name !== obj.name);

    this.selectedModel = null;
    
  }

  startRendering() {
  
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias: true });
    // this.renderer.physicallyCorrectLights = true;
    // this.renderer.outputEncoding = sRGBEncoding;
    // this.renderer.toneMapping = THREE.ReinhardToneMapping;
    // this.renderer.toneMappingExposure = 2.3;
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    let component: Model3Component = this;
    (function render() {
      component.renderer.render(component.scene, component.cameraPersp);
      requestAnimationFrame(render);
    }());


  
  }



  createControls = () => {
  
    this.orbit = new OrbitControls(this.cameraPersp, this.renderer.domElement);
    this.orbit.update();

    this.control = new TransformControls( this.cameraPersp, this.renderer.domElement );
    this.control.addEventListener( 'dragging-changed', ( event ) => {
      this.orbit.enabled = ! event['value'];

      console.log(this.selectedModel, this.control, this.orbit)


    } );

    this.orbit.addEventListener('change', () => {
      console.log('light')
      this.directionalLight.position.copy(this.currentCamera.position);
    })

    // this.interactionManager =  new InteractionManager( // Porcess
    //   this.renderer,
    //   this.currentCamera,
    //   this.renderer.domElement, false
    // );
    

  };

  // Add/remove Grid helper
  toggleGirdHelper() {
    this.isGridHelperReuired = !this.isGridHelperReuired;

    if (this.isGridHelperReuired) {
      this.scene.add( this.gridHelper );
    } else {
      this.scene.remove(this.gridHelper);
    }
  }

  // On selected model
  onSelectModel(model: any, autoFoucs?:boolean) {
    this.selectedModel = model;
    this.enableControls = true;
    console.log(this.scene)

    this.addControl();
    console.log(this.control)
    if (autoFoucs) this.updateFouvAndZoom()
    // this.setControllerMode('enableControls');
  }

  // Add control to selected obj
  addControl() {
   

    this.removeControl()

    this.control.attach(this.selectedModel);
    this.scene.add(this.control);

  }
  removeControl(){
    this.control.detach();
    this.scene.remove(this.control)
  }

  // Controller mode
  setControllerMode(mode: string) {
    this.controllerType = mode
    switch (mode) {
      case 'translate':
        this.control.setMode( 'translate' );
        break;
      case 'rotate':
        this.control.setMode( 'rotate' );
        break;
      case 'scale': // R
        this.control.setMode( 'scale' );
        break;

      default:
        break;
    }
  }

  // Enable/Disable controller
  toggleController() {
    this.enableControls = !this.enableControls;
    this.control.enabled = this.enableControls;

    this.control.showX = this.enableControls;
    this.control.showY = this.enableControls;
    this.control.showZ = this.enableControls;
  }

  

  
  // Set focus and zoom
  updateFouvAndZoom() {

    const obj: any = this.scene.getObjectByName(this.selectedModel.name);
    const  offset =  1.5;

    const boundingBox = new THREE.Box3();

    boundingBox.setFromObject( obj );

    const center = boundingBox.getCenter( new THREE.Vector3() );
    const size = boundingBox.getSize( new THREE.Vector3() );

    const startDistance = center.distanceTo(this.currentCamera.position);
      // here we must check if the screen is horizontal or vertical, because camera.fov is
      // based on the vertical direction.
    const endDistance = this.currentCamera.aspect > 1 ?
                ((size.y/2)+offset) / Math.abs(Math.tan(this.currentCamera.fov/2)) :
                ((size.y/2)+offset) / Math.abs(Math.tan(this.currentCamera.fov/2)) / this.currentCamera.aspect ;

    

    this.currentCamera.position.set(
        this.currentCamera.position.x * endDistance / startDistance,
        this.currentCamera.position.y * endDistance / startDistance,
        this.currentCamera.position.z * endDistance / startDistance,
    );
    
    this.currentCamera.near = 0.01; // fixing zoom near vaule to zoom further

      gsap.to( this.currentCamera.position, {
      delay: 0,
      duration: 2,
      ease: "power2.in",
      x: this.currentCamera.position.x ,
      y: this.currentCamera.position.y,
      z: this.currentCamera.position.z +10,
      onUpdate: () => {
        this.currentCamera.lookAt( center );
      }
    } );  
      
  }


  // Rotate parent GLB
  rotateGLBY() {
    console.log(this.model, this.scene)
    this.model.rotation.y = this.glbRotateY;
  }
  resetRotateGLBY() {
    this.glbRotateY = 0;
    this.model.rotation.y = this.glbRotateY;

  }

  // Toggle selectyed OBJ visibilty
  toggleOBJVisibility() {
    this.selectedModel.visible = !this.selectedModel.visible;
    this.enableControls = this.selectedModel.visible;
    if (this.selectedModel.visible) {
      this.addControl() 
    } else {
      this.removeControl();
    }
  }
}
